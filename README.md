# Let webmasters know that only traffic made with privacy friendly trackers is allowed

This is a brewser extension that sends a user-agent which includes a note that tracking made with programs other than Piwik are blocked.

But the other way round, if analytics is made with Piwik, then it is not blocked.

## Drawbacks

Your browser gives bits of information when the user-agent is sent to a server.
